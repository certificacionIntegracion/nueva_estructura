package qa.automated.web.bci.Runnings;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/qa/automated/web/bci/Feature", 
glue = "qa/automated/web/bci/Definitions", 
plugin = { "pretty", "html:target/cucumber" })

public class Running {
}
