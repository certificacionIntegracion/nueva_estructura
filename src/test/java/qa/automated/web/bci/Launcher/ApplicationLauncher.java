package qa.automated.web.bci.Launcher;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import qa.automated.web.bci.Generic.UsoCom;
import qa.automated.web.bci.Pages.PageLoginWeb;
import qa.automated.web.bci.Properties.PropertiesInit;
import qa.automated.web.bci.Runnings.RunningLoginWeb;

@RunWith(Suite.class)
@SuiteClasses({ RunningLoginWeb.class})
public class ApplicationLauncher {	


	public static AndroidDriver<AndroidElement> driver;
	public static WebDriver driverWeb;
	public static PropertiesInit properties;

	// PAGINAS DE LA WEB
	public static PageLoginWeb pageLoginWeb;

	@BeforeClass
	public static void setUp() {
		try {
			if (System.getenv("SELENIUM_SERVER_URL") != null && !System.getenv("SELENIUM_SERVER_URL").equalsIgnoreCase(""))
				setDriver();
			else
				setDriverDesa();
			properties = new PropertiesInit();

			// WEB
			pageLoginWeb = PageFactory.initElements(driverWeb, PageLoginWeb.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void finish() {
		System.out.println("************************************************");
		System.out.println("**   Finaliza ciclo de pruebas automatizadas  **");
		System.out.println("************************************************");
		System.out.println();
	    UsoCom.cerrarVentana(driverWeb);
	}

	public static void setDriver() throws MalformedURLException {
		DesiredCapabilities capability = null;
		switch (System.getenv("BROWSER")) {
		case "Chrome":
			capability = DesiredCapabilities.chrome();
			break;

		case "IE":
			capability = DesiredCapabilities.internetExplorer();
			capability.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
			capability.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
			capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			break;

		case "Firefox":
			capability = DesiredCapabilities.firefox();
			capability.setCapability("marionette", true);
			break;

		default:
			capability = DesiredCapabilities.chrome();
			break;
		}

		capability.setJavascriptEnabled(true);
		capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		driverWeb = new RemoteWebDriver(new URL(System.getenv("SELENIUM_SERVER_URL")), capability);
		driverWeb.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driverWeb.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
	}

	public static void setDriverDesa() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", System.getenv("BROWSER_LOCAL_DRIVER"));
		driverWeb = new ChromeDriver();
		driverWeb.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driverWeb.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		driverWeb.manage().window().maximize();
	}
}
