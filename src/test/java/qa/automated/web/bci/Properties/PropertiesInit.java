package qa.automated.web.bci.Properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class PropertiesInit {
    private String SaludoBenvenida;
	public static Properties parametros;

	public PropertiesInit() {
		Properties parametros;
		File propFileName = new File("src/test/java/qa/automated/web/bci/Config/config.properties").getAbsoluteFile();;
		FileInputStream inputStream;

		
		try {
			inputStream = new FileInputStream(propFileName);
			parametros = new Properties();
			parametros.load(inputStream);

			SaludoBenvenida = parametros.getProperty("SaludoBenvenida");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Modifica fichero de propiedades
	 * 
	 * @param fichero
	 *            Archivo properties a modificar
	 * @param key
	 *            Nombre de properties a modificar
	 * @param value
	 *            Valor que se modificara en propertie
	 * 
	 * @return void
	 */

	public String getSaludoBenvenida() {
		return SaludoBenvenida;
	}

	public void setSaludoBenvenida(String saludoBenvenida) {
		SaludoBenvenida = saludoBenvenida;
	}

	public static Properties getParametros() {
		return parametros;
	}

	public static void setParametros(Properties parametros) {
		PropertiesInit.parametros = parametros;
	}

}
