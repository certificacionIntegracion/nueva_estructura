@BCI_PageLogins
Feature: Validacion de campos rut y clave he ingresar a pantalla de usuario

  Scenario Outline: validacion seteo por puntos y guion
    When ingresa rut <rut>..
    Then valido rut ingresado con puntos y guiones..

    Examples: 
      | rut         |
      | "136355094" |

  Scenario Outline: Validar que no permita letras en campo rut
    When Ingresar letras campo rut <letras>..
    Then Comprobar que no ingresa letras..

    Examples: 
      | letras |
      | "abcd" |
      | "DFGH" |

  Scenario Outline: Validar que permita letra k en campo rut
    When Ingresar letra k campo rut <letra>..
    Then Comprobar que ingresa letra correctamente..

    Examples: 
      | letra |
      | "k"   |
      | "K"   |

  Scenario Outline: Ingreso clave invalida con cinco caracteres
    When ingreso en campo clave <clave> con cinco digitos..
    Then Valido ingreso de clave con largo de clave invalida..

    Examples: 
      | clave   |
      | "12345" |

  Scenario Outline: validacion rut y clave
    When ingreso rut <rut> y clave <clave>
    Then Se valida ingreso de clave y rut validos de forma exitosa

    Examples: 
      | rut         | clave      |
      | "102651685" | "webdesa1" |
