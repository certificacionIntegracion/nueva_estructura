package qa.automated.web.bci.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PageLoginWeb {

/************************************************************
 * Objetos con sus respectivos Get y Set
 ************************************************************/
@FindBy(how = How.ID, using = "mat-input-1")
private WebElement setPass;

@FindBy(how = How.XPATH, using = "/html/body/app-root/app-public-container/div/app-login/div/mat-card-content/div/div/div/div/app-form-login/div/form/div/button")
private WebElement SetButton;

@FindBy(how = How.XPATH, using = "//*[@placeholder='RUT']")
private WebElement SetRut;

@FindBy(how = How.XPATH, using = "/html/body/app-root/app-public-container/div/app-login/div/mat-card-content/div/div/div/div/app-form-login/div/form/mat-form-field[2]/div/div[1]/div/div[2]/span")
private WebElement BtnVer;

@FindBy(how = How.XPATH, using = "/html/body/app-root/app-public-container/div/app-login/div/mat-card-content/div/div/div/div/app-form-login/h1")
private WebElement SldoBienvenida;

	/**
	 * Getters and Setters
	 **/
	public WebElement getSetPass() {
		return setPass;
	}
	
	public WebElement getSldoBienvenida() {
		return SldoBienvenida;
	}
	
	public void setSldoBienvenida(WebElement sldoBienvenida) {
		SldoBienvenida = sldoBienvenida;
	}
	
	public WebElement getBtnVer() {
		return BtnVer;
	}
	
	public void setBtnVer(WebElement btnVer) {
		BtnVer = btnVer;
	}
	
	public void setSetPass(WebElement setPass) {
		this.setPass = setPass;
	}
	
	public WebElement getSetRut() {
		return SetRut;
	}
	
	public void setSetRut(WebElement setRut) {
		SetRut = setRut;
	}
	
	public WebElement getSetButton() {
		return SetButton;
	}
	
	public void setSetButton(WebElement setButton) {
		SetButton = setButton;
	}
	
	/************************************************************
	 * METODOS
	 ************************************************************/
	
	public void ingresarRut(String rut) {
		SetRut.clear();
		SetRut.sendKeys(rut);
	}
	
	public void ingresarClave(String rut) {
		setPass.clear();
		setPass.sendKeys(rut);
	}
	
	public void clickButtoningresar() {
		SetButton.click();
	}
	
	public boolean validarRut(String rut) {
	
		boolean validacion = true;
		try {
			rut = rut.toUpperCase();
			rut = rut.replaceAll(".", "");
			rut = rut.replace("-", "");
			int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
	
			char dv = rut.charAt(rut.length() - 1);
	
			int m = 0, s = 1;
			for (; rutAux != 0; rutAux /= 10) {
				s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = false;
			}
	
		} catch (java.lang.NumberFormatException e) {
		} catch (Exception e) {
		}
		return validacion;
	}
	
	public String obtenerRut() {
		String valor = "";
		try {
			valor = SetRut.getAttribute("value").toString().trim();
		} catch (Exception e) {
			valor = "";
		}
		return valor;
	}
	
	public boolean btnLoginActivo() {
		boolean res = false;
		try {
			res = SetButton.isDisplayed();
		} catch (Exception e) {
			res = false;
		}
		return res;
	}
	
	public boolean VerActivo() {
		boolean res = false;
		try {
			res = BtnVer.isDisplayed();
		} catch (Exception e) {
			res = false;
		}
		return res;
	}
	
	public void ClickBtnVer() {
		BtnVer.click();
	}
	
	public void clickCampoRut() {
		SetRut.click();
	}
	
	public String RetornaSldoBienvenida() {
		return SldoBienvenida.getText();
}
}
